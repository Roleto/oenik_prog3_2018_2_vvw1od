var dir_dbf6263fb15421c2fb63563b16da729b =
[
    [ "BikeShop.Data", "dir_88d0bd1456a64121bca66915b6645bff.html", "dir_88d0bd1456a64121bca66915b6645bff" ],
    [ "BikeShop.Logic", "dir_9da0ef781f0d25ed9f87d119f4fe8646.html", "dir_9da0ef781f0d25ed9f87d119f4fe8646" ],
    [ "BikeShop.Logic.Test", "dir_db0735d570ed055512d2700bec980330.html", "dir_db0735d570ed055512d2700bec980330" ],
    [ "BikeShop.Program", "dir_fcecae1dc4e082eb1968195efa56df48.html", "dir_fcecae1dc4e082eb1968195efa56df48" ],
    [ "BikeShop.Repository", "dir_b3ad7be2987b2d979179e0371b053d15.html", "dir_b3ad7be2987b2d979179e0371b053d15" ],
    [ "BikeShop.Repository.Test", "dir_42c86bb753ef4531ac8bd58dbe91c1fa.html", "dir_42c86bb753ef4531ac8bd58dbe91c1fa" ]
];