var hierarchy =
[
    [ "Attribute", null, [
      [ "BikeShop.Repository.Node", "class_bike_shop_1_1_repository_1_1_node.html", null ]
    ] ],
    [ "BikeShop.Repository.Test.Class1", "class_bike_shop_1_1_repository_1_1_test_1_1_class1.html", null ],
    [ "DbContext", null, [
      [ "BikeShop.Repository.Entity", "class_bike_shop_1_1_repository_1_1_entity.html", null ]
    ] ],
    [ "BikeShop.Repository.Extrák", "class_bike_shop_1_1_repository_1_1_extr_xC3_xA1k.html", null ],
    [ "BikeShop.Logic.ILogic", "interface_bike_shop_1_1_logic_1_1_i_logic.html", [
      [ "BikeShop.Logic.Busines_Logic", "class_bike_shop_1_1_logic_1_1_busines___logic.html", null ]
    ] ],
    [ "BikeShop.Repository.IRepository< T >", "interface_bike_shop_1_1_repository_1_1_i_repository.html", [
      [ "BikeShop.Repository.Repository< T >", "class_bike_shop_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "BikeShop.Repository.IRepository< BikeShop.Repository.Extrák >", "interface_bike_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "BikeShop.Repository.IRepository< BikeShop.Repository.ModelExtraConnect >", "interface_bike_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "BikeShop.Repository.IRepository< BikeShop.Repository.Models >", "interface_bike_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "BikeShop.Repository.IRepository< BikeShop.Repository.MotorMark >", "interface_bike_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "BikeShop.Logic.Test.LogicTest", "class_bike_shop_1_1_logic_1_1_test_1_1_logic_test.html", null ],
    [ "BikeShop.Repository.ModelExtraConnect", "class_bike_shop_1_1_repository_1_1_model_extra_connect.html", null ],
    [ "BikeShop.Repository.Models", "class_bike_shop_1_1_repository_1_1_models.html", null ],
    [ "BikeShop.Repository.MotorMark", "class_bike_shop_1_1_repository_1_1_motor_mark.html", null ],
    [ "BikeShop.Logic.Rendeles", "class_bike_shop_1_1_logic_1_1_rendeles.html", null ]
];