var class_bike_shop_1_1_repository_1_1_motor_mark =
[
    [ "MotorMark", "class_bike_shop_1_1_repository_1_1_motor_mark.html#a615c640f72b0871a7eb53ce3b9da868a", null ],
    [ "COUNTRY", "class_bike_shop_1_1_repository_1_1_motor_mark.html#ae77feabd1f0090e9e3deb07eb8ebcacd", null ],
    [ "FOUNDATION", "class_bike_shop_1_1_repository_1_1_motor_mark.html#a234ce3cf3519d47626c5a8c76c64d80e", null ],
    [ "ID", "class_bike_shop_1_1_repository_1_1_motor_mark.html#a5002e8493c773e04e3f75e1a9a89ea68", null ],
    [ "INCOME", "class_bike_shop_1_1_repository_1_1_motor_mark.html#ae3b76f7f8e55b21c0cd9bfdecf5f5142", null ],
    [ "ModelExtraConnect", "class_bike_shop_1_1_repository_1_1_motor_mark.html#aa1a125027374f510236a8140e362afb8", null ],
    [ "MODELNUMBER", "class_bike_shop_1_1_repository_1_1_motor_mark.html#a42c2623c3f5984aa13f506457fc82c1b", null ],
    [ "Models", "class_bike_shop_1_1_repository_1_1_motor_mark.html#ae1febd8470f81e0bbbeeb71ea4cddfa9", null ],
    [ "NAME", "class_bike_shop_1_1_repository_1_1_motor_mark.html#a2761a621ad44beaa297a18a6126dffdb", null ]
];