/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "bead", "index.html", [
    [ "Castle Core Changelog", "md__c_1__users__roland__documents_oenik_prog3_2018_2_vvw1od__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20063fc7ad79390bf3f93ef7c2cb4607e4.html", null ],
    [ "LICENSE", "md__c_1__users__roland__documents_oenik_prog3_2018_2_vvw1od__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_203cf990bb3a7b7b315ac22d12fed349b7.html", null ],
    [ "NUnit 3.11 - October 11, 2018", "md__c_1__users__roland__documents_oenik_prog3_2018_2_vvw1od__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_20a735609c907a355fe3e08a7d4e87f1e6.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Properties", "functions_prop.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';