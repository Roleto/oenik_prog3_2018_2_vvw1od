﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BikeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// IMplement the Repository methods
    /// </summary>
    /// <typeparam name="T"> the table</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// return a list with a T-type Table
        /// </summary>
        /// <returns> T type List </returns>
        List<T> GetAll();

        /// <summary>
        /// write tto the console the table
        /// </summary>
        void Listaz();

        /// <summary>
        /// remove a date form the datebase
        /// </summary>
        /// <param name="d">index of the date which want to delete</param>
        void Remove(string d);

        /// <summary>
        /// update the table specifike date
        /// </summary>
        /// <param name="d">index</param>
        /// <param name="mit">the rows of the updating table </param>
        /// <param name="mire">the updating value</param>
        void Update(string d, string mit, object mire);

        /// <summary>
        /// insert a date to the datebase
        /// </summary>
        /// <param name="t">the data</param>
        void Insert(T t);
    }
}