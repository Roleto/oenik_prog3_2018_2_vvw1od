﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BikeShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Repository for the tables
    /// </summary>
    /// <typeparam name="T"> T type Table </typeparam>
    public class Repository<T> : IRepository<T>
        where T : class
    {
        private readonly Entity entity = new Entity();

        /// <summary>
        /// select the T type table
        /// </summary>
        /// <returns> hte selected table into a list </returns>
        public List<T> GetAll()
        {
            return this.entity.Set<T>().Select(x => x).ToList();
        }

        /// <summary>
        /// insert to a table
        /// </summary>
        /// <param name="t">T type element </param>
        public void Insert(T t)
        {
            this.entity.Set<T>().Add(t);
            this.entity.SaveChanges();
        }

        /// <summary>
        /// write to the table data to the concole
        /// </summary>
        public void Listaz()
        {
            var q = this.GetAll();
            PropertyInfo[] infos = typeof(T).GetProperties();
            foreach (PropertyInfo item in infos)
            {
                var attr = item.GetCustomAttribute<Node>();
                if (attr != null)
                {
                    Console.Write($"{item.Name}\t");
                }
            }

            Console.WriteLine();
            foreach (var item in q)
            {
                foreach (PropertyInfo info in infos)
                {
                    var attr = info.GetCustomAttribute<Node>();
                    if (attr != null)
                    {
                        Console.Write($"{item.GetType().GetProperty(info.Name).GetValue(item, null)}\t");
                    }
                }

                Console.WriteLine();
            }
        }

        /// <summary>
        /// search an element element by d and delete it
        /// </summary>
        /// <param name="d">the index of the element</param>
        public void Remove(string d)
        {
            string idname = typeof(T).GetProperties().Select(x => x.Name).ToList().First(y => y.Contains("ID"));
            PropertyInfo info = typeof(T).GetProperty(idname);
            var parameter = Expression.Parameter(typeof(T), "x");
            var left = Expression.PropertyOrField(parameter, idname);
            var right = Expression.Constant(Convert.ChangeType(d, info.PropertyType), left.Type);
            var condition = Expression.Equal(left, right);
            var predicate = Expression.Lambda<Func<T, bool>>(condition, parameter);
            var result = this.entity.Set<T>().Single(predicate);
            this.entity.Set<T>().Remove(result);
            this.entity.SaveChanges();
        }

        /// <summary>
        /// search an element element by d and update it
        /// </summary>
        /// <param name="d">the indef of the element</param>
        /// <param name="mit">the coloum of the element wich want to update</param>
        /// <param name="mire">the value</param>
        public void Update(string d, string mit, object mire)
        {
            string idname = typeof(T).GetProperties().Select(x => x.Name).ToList().First(y => y.Contains("ID"));
            PropertyInfo info = typeof(T).GetProperty(idname);
            var parameter = Expression.Parameter(typeof(T), "x");
            var left = Expression.PropertyOrField(parameter, idname);
            var right = Expression.Constant(Convert.ChangeType(d, info.PropertyType), left.Type);
            var condition = Expression.Equal(left, right);
            var predicate = Expression.Lambda<Func<T, bool>>(condition, parameter);
            var result = this.entity.Set<T>().Single(predicate);
            result.GetType().GetProperty(mit).SetValue(result, mire);
            this.entity.SaveChanges();
        }
    }
}
