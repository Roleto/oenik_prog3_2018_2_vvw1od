﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BikeShop.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BikeShop.Logic;
    using BikeShop.Repository;
    using Moq;
    using Moq.Language;
    using NUnit.Framework;

    /// <summary>
    /// Logic text fixture
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IRepository<MotorMark>> moqMark;
        private Mock<IRepository<Models>> moqModel;
        private Mock<IRepository<Extrák>> moqExtra;
        private Mock<IRepository<ModelExtraConnect>> moqCon;

        private MotorMark mark;
        private Models model;
        private Extrák extra;
        private ModelExtraConnect con;

        private Busines_Logic logic;

        /// <summary>
        /// Setup the mock
        /// </summary>
        [SetUp]
        public void SetupTest()
        {
            this.mark = new MotorMark() { ID = 100, NAME = "test1", COUNTRY = "test1", INCOME = 1000, FOUNDATION = "test/pp/vv", MODELNUMBER = 500 };
            this.model = new Models() { ID = 100, MARK_ID = 100, NAME = "test1", REALESEDAY = "test/pp/vv", HP = 10, VOLUME = 500, BASIC_PRICE = 1000000 };
            this.extra = new Extrák() { ID = 100, FK = 100, CATEGORY = "test1", NAME = "test1", COLOR = "szin", PRICE = 1000, TBE = 0 };
            this.con = new ModelExtraConnect() { ID = 100, EXTRA_ID = 100, MODEL_ID = 100 };
            this.moqMark = new Mock<IRepository<MotorMark>>();
            this.moqModel = new Mock<IRepository<Models>>();
            this.moqExtra = new Mock<IRepository<Extrák>>();
            this.moqCon = new Mock<IRepository<ModelExtraConnect>>();
            this.moqMark.Setup(x => x.Insert(this.mark)).Verifiable();
            this.moqMark.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<object>())).Verifiable();
            this.moqMark.Setup(x => x.Remove(It.IsAny<string>())).Verifiable();
            this.moqModel.Setup(x => x.Insert(this.model)).Verifiable();
            this.moqModel.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            this.moqModel.Setup(x => x.Remove(It.IsAny<string>())).Verifiable();
            this.moqExtra.Setup(x => x.Insert(this.extra)).Verifiable();
            this.moqExtra.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            this.moqExtra.Setup(x => x.Remove(It.IsAny<string>())).Verifiable();
            this.moqCon.Setup(x => x.Insert(this.con)).Verifiable();
            this.moqCon.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            this.moqCon.Setup(x => x.Remove(It.IsAny<string>())).Verifiable();
            this.logic = new Busines_Logic(this.moqMark.Object, this.moqModel.Object, this.moqExtra.Object, this.moqCon.Object);
        }

        /// <summary>
        /// Test the insert into the mark
        /// </summary>
        [Test]
        public void Test_CreatMark()
        {
            this.moqMark.Setup(x => x.Insert(this.mark)).Verifiable();
            this.logic.Creat_Mark(this.mark);
            this.moqMark.Verify(x => x.Insert(this.mark));
        }

        /// <summary>
        /// Test the read into the mark
        /// </summary>
        [Test]
        public void Test_ReadMark()
        {
            List<MotorMark> list = new List<MotorMark>() { this.mark };
            this.moqMark.Setup(x => x.GetAll()).Returns(list);
            this.logic.Creat_Mark(this.mark);
            this.logic.Read_Mark();
            this.moqMark.Verify(x => x.Listaz());
        }

        /// <summary>
        /// Test the update into the mark
        /// </summary>
        [Test]
        public void Test_UpdateMark()
        {
            this.moqMark.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            this.logic.Creat_Mark(this.mark);
            this.logic.Update_Mark(this.mark.ID.ToString(), nameof(this.mark.NAME), "update");
            this.moqMark.Verify(x => x.Update(this.mark.ID.ToString(), nameof(this.mark.NAME), "update"));
        }

        /// <summary>
        /// Test the delete into the mark
        /// </summary>
        [Test]
        public void Test_DeleteMark()
        {
            this.moqMark.Setup(x => x.Remove(It.IsAny<string>()));
            this.logic.Creat_Mark(this.mark);
            this.logic.Delete_Mark(this.mark.ID.ToString());
            this.moqMark.Verify(x => x.Remove(this.mark.ID.ToString()));
        }

        /// <summary>
        /// Test the insert into the model
        /// </summary>
        [Test]
        public void Test_CreatModel()
        {
            this.moqModel.Setup(x => x.Insert(this.model)).Verifiable();
            this.logic.Creat_Modell(this.model);
            this.moqModel.Verify(x => x.Insert(this.model));
        }

        /// <summary>
        /// Test the read into the model
        /// </summary>
        [Test]
        public void Test_ReadModel()
        {
            List<Models> list = new List<Models>() { this.model };
            this.moqModel.Setup(x => x.GetAll()).Returns(list);
            this.logic.Creat_Modell(this.model);
            this.logic.Read_Modell();
            this.moqModel.Verify(x => x.Listaz());
        }

        /// <summary>
        /// Test the update into the model
        /// </summary>
        [Test]
        public void Test_UpdateModel()
        {
            this.moqModel.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            this.logic.Creat_Modell(this.model);
            this.logic.Update_Modell(this.model.ID.ToString(), nameof(this.model.NAME), "UPDATE");
            this.moqModel.Verify(x => x.Update(this.model.ID.ToString(), nameof(this.mark.NAME), "UPDATE"));
        }

        /// <summary>
        /// Test the delete into the model
        /// </summary>
        [Test]
        public void Test_DeleteModel()
        {
            this.moqModel.Setup(x => x.Remove(It.IsAny<string>()));
            this.logic.Creat_Modell(this.model);
            this.logic.Delete_Modell(this.model.ID.ToString());
            this.moqModel.Verify(x => x.Remove(this.model.ID.ToString()));
        }

        /// <summary>
        /// Test the insert into the Extra
        /// </summary>
        [Test]
        public void Test_CreatExtra()
        {
            this.moqExtra.Setup(x => x.Insert(this.extra)).Verifiable();
            this.logic.Creat_Extra(this.extra);
            this.moqExtra.Verify(x => x.Insert(this.extra));
        }

        /// <summary>
        /// Test the Read into the Extra
        /// </summary>
        [Test]
        public void Test_ReadExtra()
        {
            List<Extrák> list = new List<Extrák>() { this.extra };
            this.moqExtra.Setup(x => x.GetAll()).Returns(list);
            this.logic.Creat_Extra(this.extra);
            this.logic.Read_Extra();
            this.moqExtra.Verify(x => x.Listaz());
        }

        /// <summary>
        /// Test the update into the Extra
        /// </summary>
        [Test]
        public void Test_UpdateExtra()
        {
            this.moqExtra.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            this.logic.Creat_Extra(this.extra);
            this.logic.Update_Extra(this.extra.ID.ToString(), nameof(this.extra.NAME), "update");
            this.moqExtra.Verify(x => x.Update(this.extra.ID.ToString(), nameof(this.extra.NAME), "update"));
        }

        /// <summary>
        /// Test the Delete into the Extra
        /// </summary>
        [Test]
        public void Test_DeleteExtra()
        {
            this.moqExtra.Setup(x => x.Remove(It.IsAny<string>()));
            this.logic.Creat_Extra(this.extra);
            this.logic.Delete_Extra(this.extra.ID.ToString());
            this.moqExtra.Verify(x => x.Remove(this.extra.ID.ToString()));
        }

        /// <summary>
        /// Test the insert into the ModelExtraConnect
        /// </summary>
        [Test]
        public void Test_CreatCon()
        {
            this.moqCon.Setup(x => x.Insert(this.con)).Verifiable();
            this.logic.Creat_MEkapcs(this.con);
            this.moqCon.Verify(x => x.Insert(this.con));
        }

        /// <summary>
        /// Test the read into the ModelExtraConnect
        /// </summary>
        [Test]
        public void Test_ReadCon()
        {
            List<ModelExtraConnect> list = new List<ModelExtraConnect>() { this.con };
            this.moqCon.Setup(x => x.GetAll()).Returns(list);
            this.logic.Creat_MEkapcs(this.con);
            this.logic.Read_MEkapcs();
            this.moqCon.Verify(x => x.Listaz());
        }

        // [Test]
        // public void Test_UpdateCon()
        // {
        //    this.moqCon.Setup(x => x.Update(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
        //    this.logic.Creat_MEkapcs(this.con);
        //    this.logic.Update_MEkapcs(this.con.ID.ToString(), nameof(this.con.EXTRA_ID), "10");
        //    this.moqCon.Verify(x => x.Update(this.model.ID.ToString(), nameof(this.mark.NAME), "10"));
        // }

        /// <summary>
        /// Test the delete into the ModelExtraConnect
        /// </summary>
        [Test]
        public void Test_DeleteCon()
        {
            this.moqCon.Setup(x => x.Remove(It.IsAny<string>()));
            this.logic.Creat_MEkapcs(this.con);
            this.logic.Delete_MEkapcs(this.con.ID.ToString());
            this.moqCon.Verify(x => x.Remove(this.con.ID.ToString()));
        }
    }
}
