﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BikeShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using BikeShop.Logic;

    /// <summary>
    /// the main program
    /// </summary>
    public static class Program
    {
        private static void Main(string[] args)
        {
            bool kilep = true;
            while (kilep)
            {
                MEnu(out kilep);
            }
        }

        private static void MEnu(out bool kilep)
        {
            kilep = true;
            Busines_Logic logic = new Busines_Logic();
            Console.Clear();
            Console.WriteLine("-----------------------------Menü----------------------");
            Console.WriteLine("0.) Exit");
            Console.WriteLine("1.) Marka Crud");
            Console.WriteLine("2.) Modell Crud");
            Console.WriteLine("3.) Extra Crud");
            Console.WriteLine("4.) ExtraModell Crud");
            Console.WriteLine("5.) NemCrud1");
            Console.WriteLine("6.) NemCrud2");
            Console.WriteLine("7.) NemCrud3");
            Console.WriteLine("8.) Java");
            Console.WriteLine("-----------------------------Menü----------------------");
            string valasz = Console.ReadKey().KeyChar.ToString();
            switch (valasz)
            {
                case "1":
                    Console.Clear();
                    Console.WriteLine("-----------------------------Menü----------------------");
                    Console.WriteLine("0.) Vissza");
                    Console.WriteLine("1.) Listáz");
                    Console.WriteLine("2.) Hozzáad");
                    Console.WriteLine("3.) Töröl");
                    Console.WriteLine("4.) Update");
                    Console.WriteLine("-----------------------------Menü----------------------");
                    string bvalasz = Console.ReadKey().KeyChar.ToString();
                    switch (bvalasz)
                    {
                        case "1":
                            Console.Clear();
                            logic.Read_Mark();
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "2":
                            Console.Clear();
                            logic.Creat_Mark(Busines_Logic.GetCreate_mark());
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "3":
                            Console.Clear();
                            Console.Write("Id:");
                            string id = Console.ReadLine();
                            logic.Delete_Mark(id);
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "4":
                            Console.Clear();
                            Console.Write("Id:");
                            string id2 = Console.ReadLine();
                            Console.Write("mit:");
                            string mit = Console.ReadLine();
                            Console.Write("mire:");
                            string mire = Console.ReadLine();
                            logic.Update_Mark(id2, mit.ToUpper(), mire);
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "0":
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Nincs Ilyen menüpont");
                            Thread.Sleep(1500);
                            break;
                    }

                    break;
                case "2":
                    Console.Clear();
                    Console.WriteLine("-----------------------------Menü----------------------");
                    Console.WriteLine("0.) Vissza");
                    Console.WriteLine("1.) Listáz");
                    Console.WriteLine("2.) Hozzáad");
                    Console.WriteLine("3.) Töröl");
                    Console.WriteLine("4.) Update");
                    Console.WriteLine("-----------------------------Menü----------------------");
                    bvalasz = Console.ReadKey().KeyChar.ToString();
                    switch (bvalasz)
                    {
                        case "1":
                            Console.Clear();
                            logic.Read_Modell();
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "2":
                            Console.Clear();
                            logic.Creat_Modell(Busines_Logic.Create_model());
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "3":
                            Console.Clear();
                            Console.Write("Id:");
                            string id = Console.ReadLine();
                            logic.Delete_Modell(id);
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "4":
                            Console.Clear();
                            Console.Write("Id:");
                            string id2 = Console.ReadLine();
                            Console.Write("mit:");
                            string mit = Console.ReadLine();
                            Console.Write("mire:");
                            string mire = Console.ReadLine();
                            logic.Update_Mark(id2, mit.ToUpper(), mire);
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "0":
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Nincs Ilyen menüpont");
                            Thread.Sleep(1500);
                            break;
                    }

                    break;
                case "3":
                    Console.Clear();
                    Console.WriteLine("-----------------------------Menü----------------------");
                    Console.WriteLine("0.) Vissza");
                    Console.WriteLine("1.) Listáz");
                    Console.WriteLine("2.) Hozzáad");
                    Console.WriteLine("3.) Töröl");
                    Console.WriteLine("4.) Update");
                    Console.WriteLine("-----------------------------Menü----------------------");
                    bvalasz = Console.ReadKey().KeyChar.ToString();
                    switch (bvalasz)
                    {
                        case "1":
                            Console.Clear();
                            logic.Read_Extra();
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "2":
                            Console.Clear();
                            logic.Creat_Extra(Busines_Logic.Create_extra());
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "3":
                            Console.Clear();
                            Console.Write("Id:");
                            string id = Console.ReadLine();
                            logic.Delete_Extra(id);
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "4":
                            Console.Clear();
                            Console.Write("Id:");
                            string id2 = Console.ReadLine();
                            Console.Write("mit:");
                            string mit = Console.ReadLine();
                            Console.Write("mire:");
                            string mire = Console.ReadLine();
                            logic.Update_Extra(id2, mit.ToUpper(), mire);
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "0":
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Nincs Ilyen menüpont");
                            Thread.Sleep(1500);
                            break;
                    }

                    break;
                case "4":
                    Console.Clear();
                    Console.WriteLine("-----------------------------Menü----------------------");
                    Console.WriteLine("0.) Vissza");
                    Console.WriteLine("1.) Listáz");
                    Console.WriteLine("2.) Hozzáad");
                    Console.WriteLine("3.) Töröl");
                    Console.WriteLine("4.) Update");
                    Console.WriteLine("-----------------------------Menü----------------------");
                    bvalasz = Console.ReadKey().KeyChar.ToString();
                    switch (bvalasz)
                    {
                        case "1":
                            Console.Clear();
                            logic.Read_MEkapcs();
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "2":
                            Console.Clear();
                            logic.Creat_MEkapcs(Busines_Logic.Create_modelextra());
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "3":
                            Console.Clear();
                            Console.Write("Id:");
                            string id = Console.ReadLine();
                            logic.Delete_MEkapcs(id);
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "4":
                            // Console.Clear();
                            // Console.Write("Id:");
                            // string id2 = Console.ReadLine();
                            // Console.Write("mit:");
                            // string mit = Console.ReadLine();
                            // Console.Write("mire:");
                            // string mire = Console.ReadLine();
                            // logic.Update_Mark(id2, mit.ToUpper(), mire);
                            Console.WriteLine("Nem Müködik");
                            Console.Write("Enter a kilépéshez");
                            Console.ReadLine();
                            break;
                        case "0":
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Nincs Ilyen menüpont");
                            Thread.Sleep(1500);
                            break;
                    }

                    break;
                case "5":
                    Console.Clear();
                    logic.NemCrud1();
                    Console.WriteLine("Nyomj Entert a kilépéshez");
                    Console.ReadLine();
                    break;
                case "6":
                    Console.Clear();
                    logic.NemCrud2();
                    Console.WriteLine("Nyomj Entert a kilépéshez");
                    Console.ReadLine();
                    break;
                case "7":
                    Console.Clear();
                    logic.NemCrud3();
                    Console.WriteLine("Nyomj Entert a kilépéshez");
                    Console.ReadLine();
                    break;
                case "8":
                    logic.WebRequest();
                    break;
                case "0":
                    Console.Clear();
                    Console.WriteLine("Szia");
                    kilep = false;
                    Thread.Sleep(500);
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Nincs Ilyen menüpont");
                    Thread.Sleep(500);
                    break;
            }
        }
    }
}
