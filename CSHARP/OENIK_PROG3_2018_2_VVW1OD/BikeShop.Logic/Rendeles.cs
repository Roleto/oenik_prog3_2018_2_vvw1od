﻿// <copyright file="Rendeles.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BikeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A clas for the json file
    /// </summary>
    public class Rendeles
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Rendeles"/> class.
        /// </summary>
        /// <param name="iD">index</param>
        /// <param name="ark">the diference in the price</param>
        public Rendeles(int iD, int ark)
        {
            this.ID = iD;
            this.Ark = ark;
        }

        /// <summary>
        /// Gets or sets index of a modell
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the price difference
        /// </summary>
        public int Ark { get; set; }
    }
}
