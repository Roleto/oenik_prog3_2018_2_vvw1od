﻿// <copyright file="Ilogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BikeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BikeShop.Repository;

    /// <summary>
    /// implements the methoss of the busnislogic metods
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// insert a date to the MotorMark datebase
        /// </summary>
        /// <param name="mark">the data</param>
        void Creat_Mark(MotorMark mark);

        /// <summary>
        /// insert a date to the Models datebase
        /// </summary>
        /// <param name="models">the data</param>
        void Creat_Modell(Models models);

        /// <summary>
        /// insert a data to the Extrák database
        /// </summary>
        /// <param name="extrák">the data</param>
        void Creat_Extra(Extrák extrák);

        /// <summary>
        /// insert a data to the ModelExtraConnect database
        /// </summary>
        /// <param name="modelExtraConnect">the data</param>
        void Creat_MEkapcs(ModelExtraConnect modelExtraConnect);

        /// <summary>
        /// write to concole the mark
        /// </summary>
        void Read_Mark();

        /// <summary>
        /// write to concole the modell
        /// </summary>
        void Read_Modell();

        /// <summary>
        /// write to concole the Extra
        /// </summary>
        void Read_Extra();

        /// <summary>
        /// write to concole the ModelExtraConnect
        /// </summary>
        void Read_MEkapcs();

        /// <summary>
        /// update the mark table
        /// </summary>
        /// <param name="d">index of which element you want to update</param>
        /// <param name="mit">the coulume name/param>
        /// <param name="mire"> the value for what change </param>
        void Update_Mark(string d, string mit, object mire);

        /// <summary>
        /// update the MOdel table
        /// </summary>
        /// <param name="d">index of which element you want to update</param>
        /// <param name="mit">the coulume name/param>
        /// <param name="mire">The VAlue.</param>
        void Update_Modell(string d, string mit, object mire);

        /// <summary>
        /// update the mark Extra
        /// </summary>
        /// <param name="d">index of which element you want to update</param>
        /// <param name="mit">the coulume name/param>
        /// <param name="mire">the value</param>
        void Update_Extra(string d, string mit, object mire);

        /// <summary>
        /// update the ModelExtraConnect table
        /// </summary>
        /// <param name="d">index of which element you want to update</param>
        /// <param name="mit">the coulume name/param>
        /// <param name="mire">the value</param>
        void Update_MEkapcs(string d, string mit, object mire);

        /// <summary>
        /// Delete an element from the MArk Table
        /// </summary>
        /// <param name="d">the index of the element</param>
        void Delete_Mark(string d);

        /// <summary>
        /// Delete an element from the model Table
        /// </summary>
        /// <param name="d">the index of the element</param>
        void Delete_Modell(string d);

        /// <summary>
        /// Delete an element from the Extra Table
        /// </summary>
        /// <param name="d">the index of the element</param>
        void Delete_Extra(string d);

        /// <summary>
        /// Delete an element from the ModelExtraConnect Table
        /// </summary>
        /// <param name="d">the index of the element</param>
        void Delete_MEkapcs(string d);
    }
}
