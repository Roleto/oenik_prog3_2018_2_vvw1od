﻿// <copyright file="Busines_Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BikeShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Script.Serialization;
    using BikeShop.Repository;
    using Newtonsoft.Json;

    /// <summary>
    /// Logic something
    /// </summary>
    public class Busines_Logic : ILogic
    {
        private readonly IRepository<MotorMark> mark;
        private readonly IRepository<Models> model;
        private readonly IRepository<Extrák> extra;
        private readonly IRepository<ModelExtraConnect> connect;

        /// <summary>
        /// Initializes a new instance of the <see cref="Busines_Logic"/> class.
        /// </summary>
        public Busines_Logic()
        {
            this.mark = new Repository<MotorMark>();
            this.model = new Repository<Models>();
            this.extra = new Repository<Extrák>();
            this.connect = new Repository<ModelExtraConnect>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Busines_Logic"/> class.
        /// </summary>
        /// <param name="mark">mark type IRepository</param>
        /// <param name="model">model type IRepository</param>
        /// <param name="extra">extra type IRepository</param>
        /// <param name="con">con type IRepository</param>
        public Busines_Logic(IRepository<MotorMark> mark, IRepository<Models> model, IRepository<Extrák> extra, IRepository<ModelExtraConnect> con)
        {
            this.mark = mark;
            this.model = model;
            this.extra = extra;
            this.connect = con;
        }

        /// <summary>
        /// Create a MotorMark object
        /// </summary>
        /// <returns>A MotorMark </returns>
        public static MotorMark GetCreate_mark()
        {
            int id, modell, income;
            string name, country, found;
            Console.Write("MAx:999 Id: ");
            id = int.Parse(Console.ReadLine());
            Console.Write("Név: ");
            name = Console.ReadLine();
            Console.Write("Ország: ");
            country = Console.ReadLine();
            Console.Write("Max: 9999 Modell number: ");
            modell = int.Parse(Console.ReadLine());
            Console.Write("Format[yyyy/mm/dd Foundation: ");
            found = Console.ReadLine();
            Console.Write("MAxhosz:15 Income: ");
            income = int.Parse(Console.ReadLine());
            return new MotorMark() { ID = id, NAME = name, COUNTRY = country, MODELNUMBER = modell, FOUNDATION = found, INCOME = income };
        }

        /// <summary>
        /// Create a Models mark object to the insert method
        /// </summary>
        /// <returns>a Models object</returns>
        public static Models Create_model()
        {
            int id, markid, modell, hp, volume, price;
            string name, realese;
            Console.Write("MAx:999 Id: ");
            id = int.Parse(Console.ReadLine());
            Console.Write("MAx:999 Id: ");
            markid = int.Parse(Console.ReadLine());
            Console.Write("Név: ");
            name = Console.ReadLine();
            Console.Write("Max: 9999 Modell number: ");
            modell = int.Parse(Console.ReadLine());
            Console.Write("Format[yyyy/mm/dd] RealeseDay: ");
            realese = Console.ReadLine();
            Console.Write("MAxhosz:15 Income: ");
            hp = int.Parse(Console.ReadLine());
            Console.WriteLine("Löerő:");
            volume = int.Parse(Console.ReadLine());
            Console.WriteLine("PRice:");
            price = int.Parse(Console.ReadLine());
            return new Models() { ID = id, MARK_ID = markid, NAME = name, REALESEDAY = realese, HP = hp, VOLUME = volume, BASIC_PRICE = price };
        }

        /// <summary>
        /// Create a Extrák mark object to the insert method
        /// </summary>
        /// <returns>a Extrák object</returns>
        public static Extrák Create_extra()
        {
            int id, tbe, price, fk;
            string name, category, color;
            Console.Write("MAx:999 Id: ");
            id = int.Parse(Console.ReadLine());
            Console.Write("MAx:999 fk: ");
            fk = int.Parse(Console.ReadLine());
            Console.Write("Category: ");
            category = Console.ReadLine();
            Console.Write("Név: ");
            name = Console.ReadLine();
            Console.Write("Kategoria: ");
            category = Console.ReadLine();
            Console.WriteLine("Szin:");
            color = Console.ReadLine();
            Console.Write("Töbször hazsnálható(0/1): ");
            tbe = int.Parse(Console.ReadLine());
            Console.Write("Ár: ");
            price = int.Parse(Console.ReadLine());
            return new Extrák() { ID = id, FK = fk, CATEGORY = category, NAME = name, COLOR = color, PRICE = price, TBE = tbe };
        }

        /// <summary>
        /// Create a ModelExtraConnect mark object to the insert method
        /// </summary>
        /// <returns>a ModelExtraConnect object</returns>
        public static ModelExtraConnect Create_modelextra()
        {
            int id, modelid, extraid;
            Console.Write("MAx:999 Id: ");
            id = int.Parse(Console.ReadLine());
            Console.Write("MAx:999 modelId: ");
            modelid = int.Parse(Console.ReadLine());
            Console.Write("MAx:999 extraId: ");
            extraid = int.Parse(Console.ReadLine());
            return new ModelExtraConnect() { ID = id, MODEL_ID = modelid, EXTRA_ID = extraid };
        }

        /// <summary>
        /// Get the random element the java enpoint
        /// </summary>
        public async void WebRequest()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("HTTP küldése a szerver felé...");
                HttpClient client = new HttpClient();
                string response = await client.GetStringAsync("http://localhost:8080/JavaEndpoint/dolog?id=" + this.model.GetAll().Count().ToString());
                Rendeles r = new JavaScriptSerializer().Deserialize<Rendeles>(response);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <inheritdoc/>
        public void Creat_Extra(Extrák extrák)
        {
            this.extra.Insert(extrák);
        }

        /// <inheritdoc/>
        public void Creat_Mark(MotorMark mark)
        {
            this.mark.Insert(mark);
        }

        /// <inheritdoc/>
        public void Creat_MEkapcs(ModelExtraConnect modelExtraConnect)
        {
            this.connect.Insert(modelExtraConnect);
        }

        /// <inheritdoc/>
        public void Creat_Modell(Models models)
        {
            this.model.Insert(models);
        }

        /// <inheritdoc/>
        public void Delete_Extra(string d)
        {
            this.extra.Remove(d);
        }

        /// <inheritdoc/>
        public void Delete_Mark(string d)
        {
            this.mark.Remove(d);
        }

        /// <inheritdoc/>
        public void Delete_MEkapcs(string d)
        {
            this.connect.Remove(d);
        }

        /// <inheritdoc/>
        public void Delete_Modell(string d)
        {
            this.model.Remove(d);
        }

        /// <inheritdoc/>
        public void Read_Extra()
        {
            this.extra.Listaz();
        }

        /// <inheritdoc/>
        public void Read_Mark()
        {
            this.mark.Listaz();
        }

        /// <inheritdoc/>
        public void Read_MEkapcs()
        {
            this.connect.Listaz();
        }

        /// <inheritdoc/>
        public void Read_Modell()
        {
            this.model.Listaz();
        }

        /// <inheritdoc/>
        public void Update_Extra(string d, string mit, object mire)
        {
            int ki = 0;
            if (int.TryParse((string)mire, out ki))
            {
                this.extra.Update(d, mit, ki);
            }
            else
            {
                this.extra.Update(d, mit, (string)mire);
            }
        }

        /// <inheritdoc/>
        public void Update_Mark(string d, string mit, object mire)
        {
            int ki = 0;
            if (int.TryParse((string)mire, out ki))
            {
                this.mark.Update(d, mit, ki);
            }
            else
            {
                this.mark.Update(d, mit, (string)mire);
            }
        }

        /// <inheritdoc/>
        public void Update_MEkapcs(string d, string mit, object mire)
        {
            this.connect.Update(d, mit, int.Parse((string)mire));
        }

        /// <inheritdoc/>
        public void Update_Modell(string d, string mit, object mire)
        {
            int ki = 0;
            if (int.TryParse((string)mire, out ki))
            {
            this.model.Update(d, mit, ki);
            }
            else
            {
            this.model.Update(d, mit, (string)mire);
            }
        }

        /// <summary>
        /// Non Crude metode
        /// </summary>
        public void NemCrud1()
        {
            // kiirni a motorok átlagos árát márkánként
            var q = from x in this.model.GetAll()
                    group x by x.MARK_ID into g
                    from y in this.mark.GetAll()
                    where y.ID == g.Key
                    select new
                    {
                        Nev = y.NAME,
                        Avg = g.Average(x => x.BASIC_PRICE)
                    };
            Console.WriteLine("Márkak Átlag Árai:-------------------------");
            foreach (var item in q)
            {
                Console.WriteLine($"{item.Nev}\t\t{(int)item.Avg}e Ft");
            }

            Console.WriteLine("-------------------------------------------");
        }

        /// <summary>
        /// Non Crude metode
        /// </summary>
        public void NemCrud2()
        {
            // kiirni a modellek árát
            var q = from extra in this.extra.GetAll()
                    group extra by extra.FK into g
                    from con in this.connect.GetAll()
                    where g.Key == con.EXTRA_ID
                    select new
                    {
                        ID = con.MODEL_ID,
                        Ar = g.Sum(x => x.PRICE)
                    };
            var q2 = from x in q
                     from model in this.model.GetAll()
                     where model.ID == x.ID
                     select new
                     {
                         Model = model.NAME,
                         Ar = model.BASIC_PRICE + x.Ar
                     };

            foreach (var item in q2)
            {
                Console.WriteLine($"{item.Model}\t {item.Ar}");
            }
        }

        /// <summary>
        /// Non Crude metode
        /// </summary>
        public void NemCrud3()
        {
            var q = from x in this.extra.GetAll()
                    group x by x.CATEGORY into g
                    select new
                    {
                        Cat = g.Key,
                        Db = g.Key.Count()
                    };
            foreach (var item in q)
            {
                Console.WriteLine($"{item.Cat}\t{item.Db}");
            }
        }
    }
}
